(ns plf04.core)

;; Bloque de funciones para stringE

;; Función con recursividad lineal




(defn string-e-1
  [s]
  (letfn [(g [c m]
            (cond
              (and (= c \e) (>= (m :c) 3)) {:r false :c (inc (m :c))}
              (and (= c \e) (>= (m :c) 0)) {:r true :c (inc (m :c))}
              :else m))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (f (rest xs)))))]
    ((f s) :r)))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")

;; Función con recursividad de cola

(defn string-e-2
  [s]
  (letfn [(g [x]
            (and (>= x 1) (<= x 3)))
          (h [x y]
            (if (= x \e) (inc y) y))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) acc))))]
    (f s 0)))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")


;; Bloque de funciones para stringTimes
;; 
;; 
;; Función conrecursividad lineal

(defn string-times-1
  [x y]
  (letfn [(f [a b]
            (if (zero? b)
              ""
              (str a (f a (dec b)))))]
    (f x y)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

;; Función conrecursividad de cola

(defn string-times-2
  [x y]
  (letfn [(f [a b acc]
            (if (zero? b)
              acc
              (f a (dec b) (str acc a))))]
    (f x y "")))

(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

;; Bloque de funciones para frontTimes
;; 
;; 
;; Función con recursividad lineal

(defn front-times-1
  [x y]
  (letfn [(f [xx w acc h]
            (if  (zero? h)
              ""
              (cond
                (< w 3) (f (rest xx) (inc w) (str acc (first xx)) h)
                (>= w 3) (str acc (f xx w acc (dec h))))))]
    (f x  0 "" y)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

;; Función con recursividad de cola

(defn front-times-2
  [x y]
  (letfn [(f [xx y]
            (cond (> y 0) (str xx (f xx (dec y)))
                  (<= y 0) ""))
          (g [v y d acc]
            (cond
              (< d 3) (g (rest v) y (inc d) (str acc (first v)))
              (>= d 3) (f acc y)))]
    (g x y 0 "")))

(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)

;; Bloque de funciones para countXX
;; 
;; 
;; Función con recursividad lineal

(defn count-xx-1
  [x]
  (letfn [(f [a b]
            (if (empty? a)
              b
              (if (and (= \x (first a)) (= \x (first (rest a))))
                (f (rest a) (inc b))
                (f (rest a) b))))]
    (f x 0)))

(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")   
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")



;Función con recursividad de cola
(defn count-xx-2
  [x]
  (letfn [(f [a b acc]
            (if (empty? a)
              acc
              (if (and (= b (first a)) (= b (first (rest a))))
                (f (rest a) b (inc acc))
                (f (rest a) b acc))))]
    (f x \x 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "Kittens")
(count-xx-2 "")
(count-xx-2 "Kittensxxx")


;; Bloque de funciones para stringSplosion
;; 
;; 
;; Función con recursividad lineal

(defn string-splosion-1
  [x]
  (letfn [(f [a b]
            (if (or (empty? a)
                    (>= b (count a)))
              ""
              (str (subs a 0 (+ b 1)) (f a (inc b)))))]
    (f x 0)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

;; Función con recursividad de cola

(defn string-splosion-2
  [x]
  (letfn [(f [a b acc]
            (if (> b (count a))
              acc
              (f a (inc b) (str acc (subs a 0 b)))))]
    (f x 1 "")))

(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")



;; Bloque de funciones para array123

;Función con recursividad lineal
(defn array-123-1
  [xs]
  (letfn [(f [a]
            (if (empty? a)
              false
              (if (>= (count a) 3)
                (if (and (== (first a) 1) (== (first (rest a)) 2) (== (first (rest (rest a))) 3))
                  true
                  (f (rest a)))
                false)))]
    (f xs)))

(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])
;Función con recursividad de cola

(defn array-123-2
  [xs]
  (letfn [(f [a acc]
            (if (empty? a)
              false
              (if (>= (count a) 3)
                (if (and (== (first a) 1) (== (first (rest a)) 2) (== (first (rest (rest a))) 3))
                  true
                  (f (rest a) acc))
                (f (empty a) acc))))]
    (f xs [])))

(array-123-2 [1 1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [])
(array-123-2 [1])

;; Bloque de funciones para stringX
;; 
;; Función con recursividad lineal

(defn string-x-1
  [x]
  (letfn [(f [a b]
            (if (empty? a)
              ""
              (if (== 0 b)
                (str (first a) (f (rest a) (inc b)))
                (if (and (= \x (first a)) (> (count a) 1))
                  (f (rest a) (inc b))
                  (str (first a) (f (rest a) (inc b)))))))]
    (f x 0)))

(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

;Función con recursividad de cola

(defn string-x-2
  [x]
  (letfn [(f [a b acc]
            (if (empty? a)
              acc
              (if (== 0 b)
                (f (rest a) (inc b) (str acc (first a)))
                
                (if (and (= \x (first a)) (> (count a) 1))
                  (f (rest a) (inc b) acc)
                  (f (rest a) (inc b) (str acc (first a)))))))]
    (f x 0 "")))

(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")

;; Bloque de funciones para altPairs
;; 
;; 
;;Función con recursividad lineal

(defn alt-pairs-1
  [x]
  (letfn [(f [a b]
            (if (== (count a) b)
              ""
              (if (or (== b 0) (== b 1) (== b 4) (== b 5) (== b 8) (== b 9) (== b 12) (== b 13))
                (str (subs a b (+ b 1)) (f a (inc b)))
                (f a (inc b)))))]
    (f x 0))) 

(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

;;Función con recursividad de cola
(defn alt-pairs-2
  [x]
  (letfn [(f [a b acc]
            (if (== (count x) b)
              acc
              (if (or (== b 0) (== b 1) (== b 4) (== b 5) (== b 8) (== b 9) (== b 12) (== b 13))
                (f a (inc b) (str acc (subs a b (inc b))))
                (f a (inc b) acc))))]
    (f x 0 "")))

(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

;; Bloque de funciones para stringYak
;; 
;; Función con recursividad lineal
(defn string-yak-1
  [x]
  (letfn [(f [a b]
            (if (empty? a)
              ""
              (if (and (= (first a) \y) (= (first (rest (rest a))) \k) (> (count a) 1))
                (f (rest (rest (rest a))) (inc b))
                (str (first a) (f (rest a) (inc b))))))]
    (f x 0)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

;; Función con recursividad de cola

(defn string-yak-2
  [x]
  (letfn [(f [a b acc]
            (if (empty? a)
              acc
              (if (and (= (first a) \y) (= (first (rest a)) \a) (= (first (rest (rest a))) \k) (> (count a) 1))
                (f (rest (rest (rest a))) (inc b) acc)
                (f (rest a) (inc b) (str acc (first a))))))]
    (f x 0 "")))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")


;; Bloque de funciones para has271-1
;; 
;; Función con recursividad lineal

(defn has-271-1
  [xs]
  (letfn [(f [a]
            (if (empty? a)
              false
              (if (>= (count a) 3)
                (if (and (== (- (first (rest a)) (first a)) 5) (<= (if (< (- (first (rest(rest a))) (- (first a) 1)) 0)
                                                                  (* -1 (- (first (rest (rest a))) (- (first a) 1)))
                                                                  (* 1 (- (first (rest (rest a))) (- (first a) 1))))  2))
                true
                (f (rest a)))
                (f (empty a)))))]
    (f xs)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])


;; Función con recursividad de cola
(defn has-271-2
  [x]
  (letfn [(f [a acc]
             (if(empty? a)
              false
              (if (>= (count a) 3)
              (if (and (== (- (first (rest a)) (first a)) 5) (<= (if (< (- (first (rest(rest a))) (- (first a) 1)) 0)
                                                                  (* -1 (- (first (rest (rest a))) (- (first a) 1)))
                                                                  (* 1 (- (first (rest (rest a))) (- (first a) 1))))  2))
               true
               (f (rest a) acc))
                 (f (empty a) acc)
              )))]
  (f x "")))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 1])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])


